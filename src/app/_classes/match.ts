import * as firebase from "firebase";
import { Team } from "./team";
import { User } from "./user";

export class Match {
    id: string;
    createdAt: string;
    awayTeamDoc: firebase.firestore.DocumentReference;
    awayTeam: Team;
    homeTeamDoc: firebase.firestore.DocumentReference;
    homeTeam: Team;
    awayUsersDocs: firebase.firestore.DocumentReference[];
    awayUser: User;
    homeUsersDocs: firebase.firestore.DocumentReference[];
    homeUser: User;
    homeScore: number;
    awayScore: number;
}