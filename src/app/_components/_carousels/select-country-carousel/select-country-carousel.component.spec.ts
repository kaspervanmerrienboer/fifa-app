import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectCountryCarouselComponent } from './select-country-carousel.component';

describe('SelectCountryCarouselComponent', () => {
  let component: SelectCountryCarouselComponent;
  let fixture: ComponentFixture<SelectCountryCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCountryCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCountryCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
