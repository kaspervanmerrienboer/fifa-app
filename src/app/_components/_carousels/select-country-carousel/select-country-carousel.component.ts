import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Country } from '../../../_classes/country';
declare var $: any;

@Component({
  selector: 'app-select-country-carousel',
  templateUrl: './select-country-carousel.component.html',
  styleUrls: ['./select-country-carousel.component.scss']
})
export class SelectCountryCarouselComponent implements OnInit {

    @Input() id: string;
    @Input() countries: Country[];

    @Output() slide = new EventEmitter();

    constructor() {}

    ngOnInit() {
    }

    ngAfterViewInit() {
        const carousel = $('#carousel-' + this.id);

        carousel.carousel({
            interval: false
        });

        carousel.on('slid.bs.carousel', e => {
            this.slide.emit(e.relatedTarget.id);
        });
    }

}