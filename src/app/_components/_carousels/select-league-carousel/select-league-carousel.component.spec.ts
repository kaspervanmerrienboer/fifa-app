import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectLeagueCarouselComponent } from './select-league-carousel.component';

describe('SelectLeagueCarouselComponent', () => {
  let component: SelectLeagueCarouselComponent;
  let fixture: ComponentFixture<SelectLeagueCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectLeagueCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLeagueCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
