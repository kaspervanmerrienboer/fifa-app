import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import * as firebase from 'firebase';
import { League } from '../../../_classes/league';
declare var $: any;

@Component({
  selector: 'app-select-league-carousel',
  templateUrl: './select-league-carousel.component.html',
  styleUrls: ['./select-league-carousel.component.scss']
})
export class SelectLeagueCarouselComponent implements OnInit {

    @Input() id: string;
    @Input() leagues: League[];

    @Output() slide = new EventEmitter();

    leagueLogos: string[];

    constructor() {
        this.leagueLogos = new Array<string>();
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        const carousel = $('#carousel-' + this.id);

        carousel.carousel({
            interval: false
        });

        carousel.on('slid.bs.carousel', e => {
            this.getLogo(e.relatedTarget.id);
            this.slide.emit(e.relatedTarget.id);
        });
    }

    getLogo(leagueId: string){

        if(!this.leagueLogos[leagueId]){
            const storageRef = firebase.storage().ref('logos/leagues/' + leagueId + '_240x240.png');

            storageRef.getDownloadURL().then(url => {
                this.leagueLogos[leagueId] = url;
            }).catch(error => {
            });
        }
    }

}
