import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTeamCarouselComponent } from './select-team-carousel.component';

describe('SelectTeamCarouselComponent', () => {
  let component: SelectTeamCarouselComponent;
  let fixture: ComponentFixture<SelectTeamCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTeamCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTeamCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
