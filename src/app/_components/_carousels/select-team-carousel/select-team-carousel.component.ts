import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Team } from '../../../_classes/team';
declare var $: any;

@Component({
  selector: 'app-select-team-carousel',
  templateUrl: './select-team-carousel.component.html',
  styleUrls: ['./select-team-carousel.component.scss']
})
export class SelectTeamCarouselComponent implements OnInit {

    @Input() id: string;
    @Input() teams: Observable<Team[]>;

    @Output() slide = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        const carousel = $('#carousel-' + this.id);

        carousel.carousel({
          interval: false
        });

        carousel.on('slid.bs.carousel', e => {
          this.slide.emit(e.relatedTarget.id);
        });
    }

}
