import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectUserCarouselComponent } from './select-user-carousel.component';

describe('SelectUserCarouselComponent', () => {
  let component: SelectUserCarouselComponent;
  let fixture: ComponentFixture<SelectUserCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectUserCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectUserCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
