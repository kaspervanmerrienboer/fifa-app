import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { User } from '../../../_classes/user';
declare var $: any;


@Component({
  selector: 'app-select-user-carousel',
  templateUrl: './select-user-carousel.component.html',
  styleUrls: ['./select-user-carousel.component.scss']
})
export class SelectUserCarouselComponent implements OnInit {

    @Input() id: string;
    @Input() user: User[];

    @Output() slide = new EventEmitter();

    constructor() {}

    ngOnInit() {
    }

    ngAfterViewInit() {
        const carousel = $('#carousel-' + this.id);

        carousel.carousel({
            interval: false
        });

        carousel.on('slid.bs.carousel', e => {
            this.slide.emit(e.relatedTarget.id);
        });
    }

}