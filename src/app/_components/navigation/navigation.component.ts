import { Component, OnInit, Input } from '@angular/core';
import { User } from "../../_classes/user";
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Input() userObservable: Observable<User>;
  user: User;

  constructor(
      public afAuth: AngularFireAuth,
  ) { }

  ngOnInit() {
    this.userObservable.subscribe(user => {
      this.user = user;
    })
  }

  logout() {
    this.afAuth.auth.signOut();
  }

}
