import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { League } from '../../_classes/league';
import { LeaguesService } from '../../_services/leagues/leagues.service';
import { Country } from '../../_classes/country';
import { TeamsService} from '../../_services/teams/teams.service';
import { Team} from '../../_classes/team';
import { SelectCountryCarouselComponent } from '../_carousels/select-country-carousel/select-country-carousel.component';
import { SelectTeamCarouselComponent } from '../_carousels/select-team-carousel/select-team-carousel.component';
import { SelectLeagueCarouselComponent } from '../_carousels/select-league-carousel/select-league-carousel.component';

@Component({
    selector: 'app-select-match',
    templateUrl: './select-match.component.html',
    styleUrls: ['./select-match.component.scss']
})
export class SelectMatchComponent implements OnInit {

    @Input() title;
    @Input() id;
    @Input() countries: Country[];

    @ViewChild('countryCarousel') selectCountryCarouselComponent: SelectCountryCarouselComponent;
    @ViewChild('leagueCarousel') selectLeagueCarouselComponent: SelectLeagueCarouselComponent;
    @ViewChild('teamCarousel') selectTeamCarouselComponent: SelectTeamCarouselComponent;

    leaguesObservable: Observable<League[]>;
    leagues: League[];

    teamsObservable: Observable<League[]>;
    teams: Team[];

    countryId: string;
    leagueId: string;
    teamId: string;

    @Output() onChange = new EventEmitter();

    constructor(
        private leaguesService: LeaguesService,
        private teamService: TeamsService,
    ) {
    }

    ngOnInit() {
        this.countryId = this.countries[0].id;
        this.setLeagues(this.countryId);
    }

    getLeagues(countryId: string): Promise<any> {
        const promise = new Promise((resolve, reject) => {
            this.leaguesObservable = this.leaguesService.getLeagues(countryId);
            this.leaguesObservable.subscribe(leagues => {
                this.leagueId = leagues[0].id;
                this.leagues = leagues;
                this.getTeams(this.countryId, this.leagueId);
                resolve(leagues);
            });
        });

        return promise;
    }

    setLeagues(countryId: string){
        this.getLeagues(countryId).then(leagues =>{
            this.selectLeagueCarouselComponent.getLogo(leagues[0].id);
        });
    }


    getTeams(countryId: string, leagueId: string){
        const promise = new Promise((resolve, reject) => {
            this.teamsObservable = this.teamService.getTeams(countryId, leagueId);
            this.teamsObservable.subscribe(teams => {
                this.teamId = teams[0].id;
                this.teams = teams;
                resolve(teams);
                this.onChangeEmit();
            });
        });

        return promise;
    }

    setTeams(countryId: string, leagueId){
        this.getTeams(countryId, leagueId).then(teams =>{
            console.log(teams);
        });
    }

    slideCountry(countryId: string){
        this.countryId = countryId;
        this.setLeagues(countryId);
    }

    slideLeague(leagueId: string){
        this.leagueId = leagueId;
        this.setTeams(this.countryId, this.leagueId);
    }

    slideTeam(teamId: string){
        this.teamId = teamId;
        this.onChangeEmit();
    }

    onChangeEmit(){
      this.onChange.emit({ countryId: this.countryId, leagueId: this.leagueId, teamId: this.teamId });
    }
}
