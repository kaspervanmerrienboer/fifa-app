import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../../app.component';
import { NewMatchComponent } from '../../new-match/new-match.component';
import { MatchesComponent } from '../../matches/matches.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'new-match',
                component: NewMatchComponent,
            },
            {
                path: 'matches',
                component: MatchesComponent,
            },
        ]
    },

];

@NgModule({
    imports: [ RouterModule.forRoot(routes)],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
