import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Country } from '../../_classes/country';

@Injectable()
export class CountriesService {

  constructor(private afs: AngularFirestore) {
  }

  getCountries(): Observable<Country[]> {

      const countriesCollection = this.afs.collection<Country>('fifa/fifa18/countries/', ref => ref.orderBy('name'));

      const countries = countriesCollection.snapshotChanges().map(actions => {
          return actions.map(league => {
              const data = league.payload.doc.data() as Country;
              const id = league.payload.doc.id;
              return { id, ...data };
          });
      });

      return countries;
  }

}
