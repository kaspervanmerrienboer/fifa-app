import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { League } from "../../_classes/league";

@Injectable()
export class LeaguesService {

  constructor(private afs: AngularFirestore) {
  }

  getLeagues(countryId){

      console.log(countryId);
      const leaguesCollection = this.afs.collection<League>('fifa/fifa18/countries/' + countryId +'/leagues', ref => ref.orderBy('order'));

      const leagues = leaguesCollection.snapshotChanges().map(actions => {
          return actions.map(league => {
              const data = league.payload.doc.data() as League;
              const id = league.payload.doc.id;
              return { id, ...data };
          });
      });

      return leagues;
  }

}
