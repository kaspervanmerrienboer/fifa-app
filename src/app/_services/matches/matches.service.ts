import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Match } from '../../_classes/match';
import * as firebase from 'firebase';
import { Observable } from "rxjs/Observable";

@Injectable()
export class MatchesService {

    constructor(private afs: AngularFirestore) { }

    getMatches(): Observable<Match[]> {
      const matchesCollection = this.afs.collection<Match>('groups/incentro/matches/', ref => ref.orderBy('createdAt'));

      const matches = matchesCollection.snapshotChanges().map(actions => {
          return actions.map(match => {
              const data = match.payload.doc.data() as Match;
              const id = match.payload.doc.id;
              return { id, ...data };
          });
      });

      return matches;
    }

    addMatch(homeScore: number, awayScore: number, home: any, away: any){

        console.log('yolo' + homeScore + ' ' + awayScore);

        const matchesCollection = this.afs.collection<any>('groups/incentro/matches/');

        const match = {
            awayTeamDoc: firebase.firestore().doc('fifa/fifa18/countries/' + away.countryId + '/leagues/' + away.leagueId + '/teams/' + away.teamId),
            awayScore: awayScore,
            awayUsersDocs: [firebase.firestore().doc('users/C1nYSgA12dWqud9nhcdNtEVfFeN2')],
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
            homeTeamDoc: firebase.firestore().doc('fifa/fifa18/countries/' + home.countryId + '/leagues/' + home.leagueId + '/teams/' + home.teamId),
            homeScore: homeScore,
            homeUsersDocs: [firebase.firestore().doc('users/OhPQlhjDMsaLs3dUHblPScB3ewD3')],
        };

        matchesCollection.add(match).then(match => {
            console.log(match);
            console.log('succes!!!');
        }).catch(err => {
            console.log(err);
        });
    }
}
