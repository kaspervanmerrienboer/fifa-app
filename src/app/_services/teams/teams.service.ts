import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Team } from '../../_classes/team';

@Injectable()
export class TeamsService {

  constructor(private afs: AngularFirestore) {}

  getTeams(countryId, leagueId){
      const teamsCollection = this.afs.collection<Team>('fifa/fifa18/countries/' + countryId +'/leagues/' + leagueId + '/teams/', ref => ref.orderBy('name'));

      const teams = teamsCollection.snapshotChanges().map(actions => {
          return actions.map(team => {
              const data = team.payload.doc.data() as Team;
              const id = team.payload.doc.id;
              return { id, ...data };
          });
      });

      return teams;
  }
}
