import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { User } from '../../_classes/user';

@Injectable()
export class UsersService {

    constructor(private afs: AngularFirestore) {
    }

    getUser(userId: string): Observable<User> {
        const userDoc = this.afs.doc<User>('users/' + userId);
        const user = userDoc.valueChanges();

        return user;
    }

}
