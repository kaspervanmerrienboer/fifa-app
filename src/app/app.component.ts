import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { UsersService } from './_services/users/users.service';
import { User } from './_classes/user';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

    backgroundImageUrl: string;
    userObservable: Observable<User>;

    constructor(
        public afAuth: AngularFireAuth,
        public usersService: UsersService
    ) {}

    ngOnInit() {
        this.afAuth.authState.subscribe(user => {
            if(user){
                this.userObservable = this.usersService.getUser(user.uid);
            }
        });

        this.setBackgroundImage();
    }

    setBackgroundImage(){
        const storageRef = firebase.storage().ref('backgrounds/stadium.png');

        storageRef.getDownloadURL().then(url => {
            this.backgroundImageUrl = url;
        });
    }

    login() {
        this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(auth => {
            this.userObservable = this.usersService.getUser(auth.user.uid);
        });
    }
}
