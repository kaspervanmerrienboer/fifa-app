import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import * as firebase from 'firebase';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './_modules/app-routing/app-routing.module';
import { NewMatchComponent } from './new-match/new-match.component';
import { LeaguesService } from './_services/leagues/leagues.service';
import { NavigationComponent } from './_components/navigation/navigation.component';
import { SelectLeagueCarouselComponent } from './_components/_carousels/select-league-carousel/select-league-carousel.component';
import { SelectCountryCarouselComponent } from './_components/_carousels/select-country-carousel/select-country-carousel.component';
import { CountriesService } from "./_services/countries/countries.service";
import { SelectTeamCarouselComponent } from './_components/_carousels/select-team-carousel/select-team-carousel.component';
import { TeamsService } from './_services/teams/teams.service';
import { SelectMatchComponent } from './_components/select-match/select-match.component';
import { UsersService } from './_services/users/users.service';
import { SelectUserCarouselComponent } from './_components/_carousels/select-user-carousel/select-user-carousel.component';
import { MatchesComponent } from './matches/matches.component';
import { MatchesService } from './_services/matches/matches.service';

@NgModule({
  declarations: [
    AppComponent,
    NewMatchComponent,
    NavigationComponent,
    SelectLeagueCarouselComponent,
    SelectCountryCarouselComponent,
    SelectTeamCarouselComponent,
    SelectMatchComponent,
    SelectUserCarouselComponent,
    MatchesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AppRoutingModule
  ],
  providers: [
    CountriesService,
    LeaguesService,
    TeamsService,
    UsersService,
    MatchesService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
