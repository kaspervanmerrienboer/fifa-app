import { Component, OnInit } from '@angular/core';
import { MatchesService } from "../_services/matches/matches.service";
import { Observable } from "rxjs/Observable";
import { Match } from "../_classes/match";
import { Team } from "../_classes/team";
import {User} from "../_classes/user";

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnInit {

  matchesObservable: Observable<Match[]>;
  matches: Match[];

  constructor(private matchesService: MatchesService) { }

  ngOnInit() {

      this.matchesObservable = this.matchesService.getMatches();
      this.matchesObservable.subscribe(matches => {
          this.matches = matches;

          this.matches.map(match => {

            match.awayTeamDoc.onSnapshot(awayTeam => {
              match.awayTeam = awayTeam.data() as Team;
            });

            match.homeTeamDoc.onSnapshot(homeTeam => {
              match.homeTeam = homeTeam.data() as Team;
            });


            match.awayUsersDocs.map(awayUser => {
              awayUser.onSnapshot(user => {
                console.log(user.data());
                match.awayUser = user.data() as User;
              });
            });

            match.homeUsersDocs.map(homeUser => {
              homeUser.onSnapshot(user => {
                match.homeUser = user.data() as User;
              });
            });


          })

      });

  }

  getMatches(){

  }

}
