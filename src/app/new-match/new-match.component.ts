import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { League } from '../_classes/league';
import { LeaguesService } from '../_services/leagues/leagues.service';
import { Country } from '../_classes/country';
import { CountriesService } from "../_services/countries/countries.service";
import {MatchesService} from "../_services/matches/matches.service";

@Component({
  selector: 'app-new-match',
  templateUrl: './new-match.component.html',
  styleUrls: ['./new-match.component.scss']
})
export class NewMatchComponent implements OnInit {

  countriesObservable: Observable<Country[]>;
  countries: Country[];

  scoreForm: FormGroup;

  home: {
      countryId: string;
      leagueId: string;
      teamId: string;
  };

    away: {
        countryId: string;
        leagueId: string;
        teamId: string;
    };


    constructor(
    private countriesService: CountriesService,
    private matchesService: MatchesService
  ) {

    this.scoreForm = new FormGroup({
        homeScoreInput: new FormControl((''), [Validators.required]),
        awayScoreInput: new FormControl((''), [Validators.required])
    });

  }

  ngOnInit() {
      this.countriesObservable = this.countriesService.getCountries();
      this.countriesObservable.subscribe(countries => {
          this.countries = countries;
      });
  }

  setHomeTeam(team: any){
      this.home = team;
      console.log(this.home);
  }

  setAwayTeam(team: any){
      this.away = team;
      console.log(this.away);
  }


  addMatch(scoreForm: FormGroup){
      if (scoreForm.valid) {

          const value = scoreForm.value;

          this.matchesService.addMatch(value.homeScoreInput, value.awayScoreInput, this.home, this.away);
      }
  }
}
