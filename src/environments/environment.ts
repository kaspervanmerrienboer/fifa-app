// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
      apiKey: "AIzaSyBQOV9cwshh497i0T97TaA5xzbJbjzesmQ",
      authDomain: "fifa-app-753ce.firebaseapp.com",
      databaseURL: "https://fifa-app-753ce.firebaseio.com",
      projectId: "fifa-app-753ce",
      storageBucket: "fifa-app-753ce.appspot.com",
      messagingSenderId: "995201309437"
  }
};
